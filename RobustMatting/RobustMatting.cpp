// RobustMatting.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "RobustMatting.h"

int RMTable3x9[3][9] = 
{
	{0, 1, 2, 3, 4, 5, 6, 7, 8},
	{0, 0, 0, 1, 1, 1, 2, 2, 2},
	{0, 1, 2, 0, 1, 2, 0, 1, 2} 
};

cwg::RobustMatting::RobustMatting(const cv::Mat &rgbimg, const cv::Mat &trimap, double fineness) : mFineness(fineness)
{
	//cvtColor(rgbimg, origColorImage, CV_BGR2RGB);
	//cvtColor(trimap, origTrimap, CV_BGR2RGB);
	origColorImage = rgbimg;
	origTrimap = trimap;

	Lu = NULL;
	Rt = NULL;
	MKLSpSolver = NULL;

	Init();
}

cwg::RobustMatting::~RobustMatting()
{
	SAFE_DELETE(Rt);
	SAFE_DELETE(MKLSpSolver);
}

void cwg::RobustMatting::Init()
{
	mHeight          = origColorImage.rows;
	mWidth           = origColorImage.cols;

	fgdMap           = Mat(origColorImage.size(), CV_8UC1, Scalar(0));
	bgdMap           = Mat(origColorImage.size(), CV_8UC1, Scalar(0));
	uknMap           = Mat(origColorImage.size(), CV_8UC1, Scalar(0));

	initAlphaImage   = Mat(origColorImage.size(), CV_64FC1, Scalar(0.L));
	confImage        = Mat(origColorImage.size(), CV_64FC1, Scalar(1.L));

	//<creat fgd,bgd,uknMap>
	build_map();
	//</creat fgd,bgd,uknMap>

	//<get boundary, then in vector>
	get_boundary();
	//</get boundary, then in vector>

	//<select seeds>
	const int N      = 5*mFineness;
	select_seed_points(fgdSeeds, N, fgdSelectedSeeds);
	select_seed_points(bgdSeeds, N, bgdSelectedSeeds);
	//</select seeds>

	//<some other params>
	nUknowns         = countNonZero(uknMap);
	nFgd             = countNonZero(fgdMap);
	nBgd             = countNonZero(bgdMap);
	nKnowns          = nFgd + nBgd;
	//</some other params>
}

void cwg::RobustMatting::EstAlpha()
{
	int Nfseeds = fgdSelectedSeeds.size();
	int Nbseeds = bgdSelectedSeeds.size();
	int Npairs = Nfseeds*Nbseeds;

	if (initAlphaImage.empty())
		initAlphaImage = Mat(origColorImage.size(), CV_64FC1, Scalar(0.L));
	if (confImage.empty())
		confImage = Mat(origColorImage.size(), CV_64FC1, Scalar(1.L));

#pragma omp parallel for
	for (int i=0; i<mHeight; i++)
	{
#pragma omp parallel for
		for (int j=0; j<mWidth; j++)
		{
			if (fgdMap.at<uchar>(i,j) == 255)
			{//current point in fgd
				initAlphaImage.at<double>(i,j) = 1.0;				
			}
			if (bgdMap.at<uchar>(i,j) == 255)
			{//current point in fgd
				//do nothing;
			}
			if (uknMap.at<uchar>(i,j) == 255)
			{//current point in fgd
				vector<ConfAlpha> vecConfAlpha(Npairs);
				Vec3b curColor = origColorImage.at<Vec3b>(i,j);
				//<find the min distance from current point to the fgd and bgd>
				double min_distance_to_fgd_sq = min_distance_to_fbgd(curColor, fgdSelectedSeeds);
				double min_distance_to_bgd_sq = min_distance_to_fbgd(curColor, bgdSelectedSeeds);
				//</find the min distance from current point to the fgd and bgd>

				//<find the best linear combination of fgd/bgd pairs>
				int k = 0;
				for (int i=0; i<Nfseeds; i++)
				{
					Vec3b fgdColor = fgdSelectedSeeds[i].origcolor;
					for (int j=0; j<Nbseeds; j++)
					{
						Vec3b bgdColor = bgdSelectedSeeds[j].origcolor;
						double alphatmp = est_alpha(curColor, fgdColor, bgdColor);
						double rdval_sq = rd_sq(curColor, fgdColor, bgdColor, alphatmp);
						double wF = weight_to_xgd(curColor, fgdColor, min_distance_to_fgd_sq);
						double wB = weight_to_xgd(curColor, bgdColor, min_distance_to_bgd_sq);

						double conftmp = confidence(rdval_sq, wF, wB);
						vecConfAlpha[k].conf = conftmp;
						vecConfAlpha[k].alpha = alphatmp;
						k++;
					}
				}
				//</find the best linear combination of fgd/bgd pairs>
				
				//<remove wrong result>
				for (int i=0; i<Npairs; i++)
				{
					double tmp = vecConfAlpha[i].alpha;
					if (tmp < -0.05)
						vecConfAlpha[i].conf = 0.L;
					if (tmp > +1.05)
						vecConfAlpha[i].conf = 0.L;
					if (tmp >= -0.05 && tmp < 0.0)
						vecConfAlpha[i].alpha = 0.0;
					if (tmp <=1.05 && tmp > 1.0)
						vecConfAlpha[i].alpha = 1.0;
				}
				//</remove wrong result>

				//<sort and select the best 3 in vecConfAlpha>
				vector<int> indexforsort(Npairs);
				for (int i=0; i<Npairs; i++)
					indexforsort[i] = i;

				for (int i=3; i<Npairs; i++)
				{
					for (int k=0; k<3; k++)
					{
						if (vecConfAlpha[indexforsort[i]].conf > vecConfAlpha[indexforsort[k]].conf)
						{
							std::swap(indexforsort[i], indexforsort[k]);
						}
					}
				}

				double _alpha = (vecConfAlpha[indexforsort[0]].alpha 
					+ vecConfAlpha[indexforsort[1]].alpha 
					+ vecConfAlpha[indexforsort[2]].alpha)/3.L;
				double _conf = (vecConfAlpha[indexforsort[0]].conf 
					+ vecConfAlpha[indexforsort[1]].conf 
					+ vecConfAlpha[indexforsort[2]].conf)/3.L;

				initAlphaImage.at<double>(i,j) = _alpha;
				confImage.at<double>(i,j) = _conf;
			}
		}
	}
	
}

void cwg::RobustMatting::BuildMatrix()
{
	MKLSpSolver = new SparseSolver(nUknowns);

	Lu = MKLSpSolver->A;//new Sparse_Matrix(nUknowns, nUknowns, SYM_LOWER, false, CCS);//ukn~ukn
	
	Rt = new Sparse_Matrix(nUknowns, nKnowns+2);//ukn~kn
	Rt->begin_fill_entry();
	
	double* sum_of_rows_Lu = new double [nUknowns];
	double* sum_of_rows_Rt = new double [nUknowns];
	memset(sum_of_rows_Lu, 0, nUknowns*sizeof(double));
	memset(sum_of_rows_Rt, 0, nUknowns*sizeof(double));

	Mat erodeKnMap, middleMap;
	erode(knMap, erodeKnMap, Mat());
	middleMap = erodeKnMap(Range(1, mHeight-1), Range(1, mWidth-1));
	unsigned int Ntmp = (mHeight-2)*(mWidth-2) - countNonZero(middleMap);
	unsigned int tlen = Ntmp * 36;
	Mat rowidx(tlen, 1, CV_32SC1, Scalar(0));
	Mat colidx(tlen, 1, CV_32SC1, Scalar(0));
	Mat vars(tlen, 1, CV_64FC1, Scalar(0.L));
	int len = 0;

	int* tmparray1 = new int [mHeight*mWidth];
	for (int i=0; i<mHeight*mWidth; i++)
		tmparray1[i] = i;
	Mat indexMap(mHeight, mWidth, CV_32SC1, tmparray1);

	
	//<big loop>
	#pragma omp parallel for
	for (int i=1; i<mHeight-1; i++)
	{
		#pragma omp parallel for
		for (int j=1; j<mWidth-1; j++)
		{
			Mat win_flagidx(3, 3, CV_32SC1);
			Mat winI(3, 3, CV_8UC3);
			Mat A9x3(9, 3, CV_64FC1, Scalar(0.L));
			Mat win_mu(3, 1, CV_64FC1, Scalar(0.L));
			Mat win_var(3, 3, CV_64FC1, Scalar(0.L));
			const double epsilon = 1e-7;
			Mat T9x9(9, 9, CV_64FC1, Scalar(0.L));

			Mat M9x3, tmp1, tmp2, tmp3, tmp4;

			if (erodeKnMap.at<uchar>(i,j) == 255)
				continue;
			
			win_flagidx = flagidxMap( Range(i-1, i+2), Range(j-1, j+2) );

			winI = origColorImage(Range(i-1, i+2), Range(j-1, j+2));
			build_mat_A9x3(winI, A9x3);
			win_mu = mean_of_A9x3(A9x3);//win_mu: 3r 1c

			M9x3 = build_mat_M9x3(win_mu);

			tmp1 = A9x3.t()*A9x3/9.0;
			tmp2 = win_mu*win_mu.t();
			tmp3 = epsilon/9.0*Mat::eye(3, 3, CV_64FC1);
			double flaginv = invert( tmp1-tmp2+tmp3, win_var);
			
			tmp4 = A9x3 - M9x3;
			T9x9 = (1.0 + tmp4*win_var*tmp4.t()) / 9;	

			//<install the vals in T9x9 into Lu, Rt>
			for (int i=1; i<9; i++)
			{
				for (int j=0; j<i; j++)
				{//j < i: IdxJ < IdxI
					int IdxI = win_flagidx.at<int>(RMTable3x9[1][i], RMTable3x9[2][i]);
					int IdxJ = win_flagidx.at<int>(RMTable3x9[1][j], RMTable3x9[2][j]);
					//int Ib0 = IdxI - 1;
					if (IdxI>0 && IdxJ>0)//both kn => no need process
						continue;
				    
					if (IdxI<0 && IdxJ<0)
					{//both ukn
						int I = -IdxI-1, J = -IdxJ-1; // I > J
						assert(I > J);
						double tmp = T9x9.at<double>(i,j);
						if (I == 3 && J == 2)
						{
							int _k_k_ = 0;
							_k_k_++;
						}
						Lu->fill_entry(I, J, (double)(-tmp));
						sum_of_rows_Lu[I] += (double)tmp;
						sum_of_rows_Lu[J] += (double)tmp;
					}
					if (IdxI<0 && IdxJ>0)
					{//I~ukn J~kn
						int I = -IdxI-1, J = IdxJ-1;
						double tmp = T9x9.at<double>(i,j);
						Rt->fill_entry(I, J, (double)(-tmp));
						sum_of_rows_Rt[I] += (double)tmp;
					}
					if (IdxI>0 && IdxJ<0)
					{//I~kn J~ukn
						int I = IdxI-1, J = -IdxJ-1;
						double tmp = T9x9.at<double>(i,j);
						Rt->fill_entry(J, I, (double)(-tmp));
						sum_of_rows_Rt[J] += (double)tmp;
					}
				}
			}
			//</install the vals in T9x9 into Lu, Rt>	
		}
	}
	//<big loop>
	
	//<compute the rest of Rt: ukn~F/B>
	const double gamma = 1e-7;
	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			int Idx = flagidxMap.at<int>(i,j);
			if (Idx < 0)
			{//current pixel is ukn
				double _alpha = initAlphaImage.at<double>(i,j);
				if (_alpha>0.5)
				{
					double _conf = confImage.at<double>(i,j);
					double tmp5 = -gamma*(_conf*_alpha+1.L-_conf);
					double tmp6 = -gamma*_conf*(1.L-_alpha);
					Rt->set_entry(-Idx-1, nKnowns, tmp5);
					Rt->set_entry(-Idx-1, nKnowns+1, tmp6);
					sum_of_rows_Rt[-Idx-1] += gamma;
				}
				else
				{
					double _conf_ = confImage.at<double>(i,j);
					double tmp5 = -gamma*_conf_*_alpha;
					double tmp6 = -gamma*(_conf_*(1.L-_alpha)+(1.L-_conf_));
					Rt->set_entry(-Idx-1, nKnowns, tmp5);
					Rt->set_entry(-Idx-1, nKnowns+1, tmp6);
					sum_of_rows_Rt[-Idx-1] += gamma;
				}
			}
		}
	}
	//</compute the rest of Rt: ukn~F/B>

	//<sum_of_Rt => sum_of_Lu>
	for (int i=0; i<nUknowns; i++)
		sum_of_rows_Lu[i] += sum_of_rows_Rt[i];
	//</sum_of_Rt => sum_of_Lu>

	//<install the diag of Lu>
	for (int i=0; i<nUknowns; i++)
		Lu->set_entry(i, i, sum_of_rows_Lu[i]);
	//</install the diag of Lu>

	neg_RtxAk = MKLSpSolver->b;
	
	double* neg_Ak = new double [nKnowns+2];
	memset(neg_Ak, 0, sizeof(double)*(nKnowns+2));
	int _k = 0;
	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			if (flagidxMap.at<int>(i,j) > 0)
			{
				if (fgdMap.at<uchar>(i,j) == 255)
					neg_Ak[_k] = -1.0;
				//else if (bgdMap.at<uchar>(i,j) == 255)
				//	neg_Ak[_k++] = 0.0;
				//else
				//	;
				_k++;
			}
		}
	}
	neg_Ak[_k++] = -1.0;
	neg_Ak[_k++] = 0.0;
	
	Rt->end_fill_entry();
	multiply(Rt, neg_Ak, neg_RtxAk);

	indexMap.release();
	SAFE_DELETE_ARRAY(tmparray1);//delete [] tmparray1;
	SAFE_DELETE_ARRAY(sum_of_rows_Lu);//delete [] sum_of_rows_Lu;
	SAFE_DELETE_ARRAY(sum_of_rows_Rt);//delete [] sum_of_rows_Rt;
	SAFE_DELETE_ARRAY(neg_Ak);//delete [] neg_Ak;
}

void cwg::RobustMatting::Solve()
{
	//<Final solve>
	double* Au = MKLSpSolver->x;
	int k = 0;
	for (int i=0; i<initAlphaImage.rows; i++)
	{
		for (int j=0; j<initAlphaImage.cols; j++)
		{
			if (uknMap.at<uchar>(i, j))
				Au[k++] = initAlphaImage.at<double>(i,j);
		}
	}
	MKLSpSolver->Run(mFineness*200);

	for(int i=0; i<nUknowns; i++)
	{
		if (Au[i]<0.02)
			Au[i] = 0;
		if (Au[i]>0.98)
			Au[i] = 1;
	}
	//</Final solve>

	store(Au);
}

void cwg::RobustMatting::SaveInternalImage(int whichone)
{
	switch (whichone)
	{
	case 0:
		imwrite("OrigColorImage.bmp", origColorImage);break;
	case 1:
		imwrite("Trimap.bmp", origTrimap);break;
	case 2:
		imwrite("InitAlphaImage.bmp", initAlphaImage*255.);break;
	case 3:
		imwrite("ConfImage.bmp", confImage*255.);break;
	case 4:
		imwrite("finalAlphaImage.bmp", finalAlphaImage);break;
	case 5:
		break;
	}
}

void cwg::RobustMatting::select_seed_points(const vector<SeedPoint>& seeds, int N, vector<SeedPoint>& selected_points )
{
	int _size = seeds.size();
	int _t = _size / N;
	
	selected_points = vector<SeedPoint>(N);
	for (int i=0; i<N; i++)
	{
		selected_points[i] = seeds[i*_t];
	}
}

void cwg::RobustMatting::build_map()
{
	Mat r(origTrimap.size(), CV_8UC1);
	cvtColor(origTrimap, r, CV_RGB2GRAY);
	//imwrite("r.bmp", r);

	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			uchar tmp = r.at<uchar>(i,j);
			if (tmp == 255)//fgd
				fgdMap.at<uchar>(i,j) = 255;
			else
				if (tmp == 0)//bgd
					bgdMap.at<uchar>(i,j) = 255;
				else//ukn
					uknMap.at<uchar>(i,j) = 255;
		}
	}

	knMap = fgdMap + bgdMap;

	//<build flagmap for building matrix>
	flagidxMap = Mat(origColorImage.size(), CV_32SC1);

	int uknidx=-1, knidx = +1;
	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			if (uknMap.at<uchar>(i,j) == 255)
			{
				flagidxMap.at<int>(i,j) = uknidx;//flagMap[i][j].flag = false;
				uknidx--;//flagMap[i][j].nth = uknidx++;
			}
			else
			{
				flagidxMap.at<int>(i,j) = knidx;//flagMap[i][j].flag = true;
				knidx++;//flagMap[i][j].nth = knidx++;
			}
		}
	}

}

void cwg::RobustMatting::get_boundary()
{
	Mat tmp1, tmp2, tmp3;
	erode(fgdMap, tmp1, Mat());
	erode(bgdMap, tmp2, Mat());
	erode(uknMap, tmp3, Mat());
	fgdBdy = fgdMap - tmp1;
	bgdBdy = bgdMap - tmp2;
	uknBdy = uknMap - tmp3;

	/*imwrite("fgdBdy.bmp", fgdBdy);
	imwrite("bgdBdy.bmp", bgdBdy);
	imwrite("uknBdy.bmp", uknBdy);*/

	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			if (fgdBdy.at<uchar>(i,j) == 255)
			{
				SeedPoint fseed = {i, j, i*mWidth+j, origColorImage.at<Vec3b>(i,j), 1.0};
				fgdSeeds.push_back(fseed);
			}
			if (bgdBdy.at<uchar>(i,j) == 255)
			{
				SeedPoint bseed = {i, j, i*mWidth+j, origColorImage.at<Vec3b>(i,j), 0.0};
				bgdSeeds.push_back(bseed);
			}
		}
	}
}

inline double cwg::RobustMatting::color_diff(Vec3b c1, Vec3b c2)// |color1 - color2|^2
{
	Vec3d cdiff = (Vec3d)(c1) - (Vec3d)(c2);
	double tmp = cdiff[0]*cdiff[0] + cdiff[1]*cdiff[1] + cdiff[2]*cdiff[2];
	return tmp;
}

inline double cwg::RobustMatting::min_distance_to_fbgd(Vec3b curcolor, const vector<SeedPoint>& selectedseeds)
{
	int N = selectedseeds.size();
	double *dis = new double [N];
	for (int i=0; i<N; i++)
	{
		dis[i] = color_diff(curcolor, selectedseeds[i].origcolor);
	}
	double tmp = (*min_element(dis, dis+N))+1e-5;
	delete [] dis;
	return tmp;
}

inline double cwg::RobustMatting::est_alpha(Vec3b curColor, Vec3b fgdColor, Vec3b bgdColor)
{
	Vec3d C_B = (Vec3d)(curColor) - (Vec3d)(bgdColor);
	Vec3d F_B = (Vec3d)(fgdColor) - (Vec3d)(bgdColor);
	double tmp1 = C_B.dot(F_B);
	double tmp2 = F_B[0]*F_B[0] + F_B[1]*F_B[1] + F_B[2]*F_B[2];
	return (double)(tmp1 / tmp2);
}

inline double cwg::RobustMatting::rd_sq(Vec3b curColor, Vec3b fgdColor, Vec3b bgdColor, double alphatmp)
{
	Vec3d tmp1 = (Vec3d)(curColor) - ((Vec3d)(fgdColor))*alphatmp - ((Vec3d)(bgdColor))*(1.0-alphatmp);
	Vec3d tmp2 = (Vec3d)(fgdColor) - (Vec3d)(bgdColor);
	double tmp = (tmp1[0]*tmp1[0] + tmp1[1]*tmp1[1] + tmp1[2]*tmp1[2]) 
				/ (tmp2[0]*tmp2[0] + tmp2[1]*tmp2[1] + tmp2[2]*tmp2[2]);
	return tmp;
}

inline double cwg::RobustMatting::weight_to_xgd(Vec3b curColor, Vec3b fbgdColor, double min_distance_to_fbgd_sq)
{
	Vec3d FB_C = (Vec3d)(fbgdColor) - (Vec3d)(curColor);
	double tmp = (FB_C[0]*FB_C[0]+FB_C[1]*FB_C[1]+FB_C[2]*FB_C[2]) / min_distance_to_fbgd_sq;
	return std::exp(-tmp);
}

inline double cwg::RobustMatting::confidence(double rdval_sq, double wF, double wB, double sigma_sq)
{
	return (std::exp(-rdval_sq*wF*wB/sigma_sq));
}

inline void cwg::RobustMatting::build_mat_A9x3(const Mat& winI, Mat& A9x3)
{
	if (winI.isContinuous())
	{
		A9x3 = winI.reshape(1, 9);
		A9x3 /= 255.0;
	}
	else
	{
		Mat matarray[3];
		split(winI, matarray);
		for (int k=0; k<3; k++)
		{
			int t = 0;
			for (int i=0; i<3; i++)
			{
				for (int j=0; j<3; j++)
				{
					A9x3.at<double>(t,k) = (double)(matarray[k].at<uchar>(i,j))/255.0;
					t++;
				}
			}
		}
	}
}

inline Mat cwg::RobustMatting::build_mat_M9x3(const Mat& win_mu)
{
	//double* tmp = new double [27];
	//for (int i=0; i<9; i++)
	//{
	//	tmp[3*i] = win_mu.at<double>(0,0);
	//	tmp[3*i+1] = win_mu.at<double>(1,0);
	//	tmp[3*i+2] = win_mu.at<double>(2,0);
	//}
	Mat M9x3(9, 3, CV_64FC1);
	double* ptr = (double*)M9x3.data;
	for (int i=0; i<9; i++)
		memcpy(ptr+3*i, win_mu.data, sizeof(double)*3);
	//delete [] tmp;
	return M9x3;
}

inline Mat cwg::RobustMatting::mean_of_A9x3(const Mat& A9x3)
{
	Mat tmp(3,1,CV_64FC1);
	tmp.at<double>(0,0) = mean(A9x3.col(0))[0];
	tmp.at<double>(1,0) = mean(A9x3.col(1))[0];
	tmp.at<double>(2,0) = mean(A9x3.col(2))[0];
	return tmp;
}

inline void cwg::RobustMatting::store(double* Au)
{
	//<Store alphas>
	fgdMap.convertTo(finalAlphaImage, CV_64FC1);
	for (int i=0; i<mHeight; i++)
	{
		for (int j=0; j<mWidth; j++)
		{
			int IdxI = flagidxMap.at<int>(i,j);
			if (IdxI < 0)
				finalAlphaImage.at<double>(i,j) = 255.0*Au[-IdxI-1];
		}
	}
	
	finalAlphaImage.convertTo(finalAlphaImage, CV_8UC1);
	//</Store alphas>
}

void cwg::RobustMatting::RunMatting()
{
	//SaveInternalImage(0);
	//SaveInternalImage(1);
	
	EstAlpha(); 
	//SaveInternalImage(2);
	//SaveInternalImage(3);

	BuildMatrix();
	Solve();
	//SaveInternalImage(4);
}
