#ifndef _CWG_ROBUSTMATTING_H_
#define _CWG_ROBUSTMATTING_H_
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the ROBUSTMATTING_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// ROBUSTMATTING_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef ROBUSTMATTING_EXPORTS
#define ROBUSTMATTING_API __declspec(dllexport)
#else
#define ROBUSTMATTING_API __declspec(dllimport)
#endif

#include "cwgLibs.h"

struct SeedPoint 
{
	int															rowidx;
	int															colidx;
	int															fullidx;
	Vec3b														origcolor;
	double														alpha;
};

struct ConfAlpha
{
	double														conf;
	double														alpha;
};

namespace cwg
{
	class ROBUSTMATTING_API RobustMatting
	{
	public:
		RobustMatting(const Mat& rgbimg, const Mat& trimap, double fineness=1.0);
		~RobustMatting();

		void													RunMatting();

		void													Init();
		void													EstAlpha();
		void													BuildMatrix();
		void													Solve();

		inline Mat												GetFinalResult() const {return finalAlphaImage;}

		void													SaveInternalImage(int whichone);

	public://data
		SparseSolver*											MKLSpSolver;
		Sparse_Matrix*											Lu; // <== not by new, just given by MKLSpSolver
		Sparse_Matrix*											Rt;
		double*													neg_RtxAk; // <== not by new, just given by MKLSpSolver

		int														nUknowns;
		int														nKnowns;
		int														nFgd;
		int														nBgd;

	private://functions:
		void													select_seed_points(const vector<SeedPoint>& seeds, int N, vector<SeedPoint>& selected_points);
		void													build_map();//Build fgd, bgd and ukn maps
		void													get_boundary();//Get boundary, then store them in vector

		inline double											color_diff(Vec3b c1, Vec3b c2);// |color1 - color2|^2
		inline double											min_distance_to_fbgd(Vec3b curcolor, const vector<SeedPoint>& selectedseeds);
		inline double											est_alpha(Vec3b curColor, Vec3b fgdColor, Vec3b bgdColor);
		inline double											rd_sq(Vec3b curColor, Vec3b fgdColor, Vec3b bgdColor, double alphatmp);
		inline double											weight_to_xgd(Vec3b curColor, Vec3b fbgdColor, double min_distance_to_fbgd_sq);
		inline double											confidence(double rdval_sq, double wF, double wB, double sigma_sq=0.0000001);
		inline void												build_mat_A9x3(const Mat& winI, Mat& A9x3);
		inline Mat												build_mat_M9x3(const Mat& win_mu);

		inline void												store(double* Au);
		inline Mat												mean_of_A9x3(const Mat& A9x3);

	private:
		Mat														origColorImage;
		Mat														origTrimap;

		Mat														fgdMap;
		Mat														bgdMap;
		Mat														uknMap;
		Mat														knMap;
		Mat														flagidxMap;


		Mat														fgdBdy;
		Mat														bgdBdy;
		Mat														uknBdy;

		vector<SeedPoint>										fgdSeeds;
		vector<SeedPoint>										bgdSeeds;

		vector<SeedPoint>										fgdSelectedSeeds;
		vector<SeedPoint>										bgdSelectedSeeds;


		Mat														initAlphaImage;			//double 0.0~1.0
		Mat														finalAlphaImage;		//double 0.0~255.0
		Mat														confImage;				//double 0.0~1.0

		int														mWidth;
		int														mHeight;

		double													mFineness;				//used for controlling the fineness of the algorithm.

	};
}



#endif