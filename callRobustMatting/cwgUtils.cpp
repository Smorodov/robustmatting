#include "cwgUtils.h"

uchar  array_colormap_jet[192] = 
{
	0x8F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0xAF, 0x00, 0x00, 0xBF, 0x00, 0x00, 0xCF, 0x00, 0x00, 0xDF, 0x00, 0x00, 0xEF, 0x00, 0x00, 0xFF, 0x00, 0x00, 
	0xFF, 0x10, 0x00, 0xFF, 0x20, 0x00, 0xFF, 0x30, 0x00, 0xFF, 0x40, 0x00, 0xFF, 0x50, 0x00, 0xFF, 0x60, 0x00, 0xFF, 0x70, 0x00, 0xFF, 0x80, 0x00, 
	0xFF, 0x8F, 0x00, 0xFF, 0x9F, 0x00, 0xFF, 0xAF, 0x00, 0xFF, 0xBF, 0x00, 0xFF, 0xCF, 0x00, 0xFF, 0xDF, 0x00, 0xFF, 0xEF, 0x00, 0xFF, 0xFF, 0x00, 
	0xEF, 0xFF, 0x10, 0xDF, 0xFF, 0x20, 0xCF, 0xFF, 0x30, 0xBF, 0xFF, 0x40, 0xAF, 0xFF, 0x50, 0x9F, 0xFF, 0x60, 0x8F, 0xFF, 0x70, 0x80, 0xFF, 0x80,
	0x70, 0xFF, 0x8F, 0x60, 0xFF, 0x9F, 0x50, 0xFF, 0xAF, 0x40, 0xFF, 0xBF, 0x30, 0xFF, 0xCF, 0x20, 0xFF, 0xDF, 0x10, 0xFF, 0xEF, 0x00, 0xFF, 0xFF, 
	0x00, 0xEF, 0xFF, 0x00, 0xDF, 0xFF, 0x00, 0xCF, 0xFF, 0x00, 0xBF, 0xFF, 0x00, 0xAF, 0xFF, 0x00, 0x9F, 0xFF, 0x00, 0x8F, 0xFF, 0x00, 0x80, 0xFF, 
	0x00, 0x70, 0xFF, 0x00, 0x60, 0xFF, 0x00, 0x50, 0xFF, 0x00, 0x40, 0xFF, 0x00, 0x30, 0xFF, 0x00, 0x20, 0xFF, 0x00, 0x10, 0xFF, 0x00, 0x00, 0xFF, 
	0x00, 0x00, 0xEF, 0x00, 0x00, 0xDF, 0x00, 0x00, 0xCF, 0x00, 0x00, 0xBF, 0x00, 0x00, 0xAF, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x8F, 0x00, 0x00, 0x80 
};
extern Mat glbColorMapJet = Mat(64, 1, CV_8UC3, array_colormap_jet);

// System IO
vector<string> show_file_list(const string &folder_name, const string &suffix) 
{
	vector<string> names;
	char search_path[500];
	sprintf(search_path, "%s*.%s", folder_name.c_str(), suffix.c_str());
	WIN32_FIND_DATA fd; 
	HANDLE hFind = ::FindFirstFile(search_path, &fd); 
	if(hFind != INVALID_HANDLE_VALUE) 
	{ 
		do 
		{ 
			// read all (real) files in current folder, delete '!' read other 2 default folder . and ..
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				names.push_back(fd.cFileName);
			}
		}while(::FindNextFile(hFind, &fd)); 
		::FindClose(hFind); 
	} 
	return names;
}

vector<string> show_subdir_list(const string& folder_name, const string& subfolder_prefix)
{
	vector<string> subfolder_list;
	int lens = subfolder_prefix.size();
	int len;
	struct dirent* dir;
	DIR* Dir;

	Dir = opendir(folder_name.c_str());
	if (Dir != NULL)
	{
		while ((dir = readdir(Dir))!=NULL)
		{
			len = strlen(dir->d_name);
			if (len < lens)
				continue;
			if (dir->d_type != DT_DIR)
				continue;
			if (strncmp(dir->d_name, subfolder_prefix.c_str(), lens) == 0)
			{
				subfolder_list.push_back(dir->d_name);
			}
		}
	}
	return subfolder_list;
}

bool is_file_exist(const string& filename)
{
	ifstream ifile(filename, ios::binary);
	bool file_exist = ifile.is_open();
	if (file_exist)
		ifile.close();
	return file_exist;
}

int file_length( const string& filename )
{
	ifstream ifile(filename, ios::binary);
	if (!ifile.is_open())
		return -1;

	ifile.seekg(0, ifile.end);
	int tmp = ifile.tellg();
	ifile.close();
	return tmp;
}

string replace_suffix_name( string inputfilename, string suffix )
{
	unsigned found = inputfilename.find_last_of(".");
	string newname = inputfilename.substr(0, found) + suffix;
	return newname;
}

// Math
double dround(double r)
{
	return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}

// Other
void pexit(string errorinfo)
{
	cout << errorinfo << endl;
	system("pause");
	exit(0);
}

// OpenCV
cv::Mat imchessboard( int height, int width )
{
	Mat I(height, width, CV_8UC3);

	Mat Chessboard_BgdImage(2, 2, CV_8UC3, Scalar::all(0));
	Chessboard_BgdImage.at<Vec3b>(0,0) = Vec3b(255, 255, 255);
	Chessboard_BgdImage.at<Vec3b>(1,1) = Vec3b(255, 255, 255);
	Chessboard_BgdImage.at<Vec3b>(0,1) = Vec3b(128, 128, 128);
	Chessboard_BgdImage.at<Vec3b>(1,0) = Vec3b(128, 128, 128);

	for (int i=0; i<height; i++)
	{
		for (int j=0; j<width; j++)
		{
			I.at<Vec3b>(i,j) = Chessboard_BgdImage.at<Vec3b>(i%16/8, j%16/8);
		}
	}
	return I;
}

cv::Mat loadmat( const string& filename )
{
	ifstream ifile;
	ifile.open( filename.c_str() , ios::binary );

	Mat kMatrix = loadmat(ifile);

	ifile.close();

	return kMatrix;
}

cv::Mat loadmat( ifstream& ifile )
{
	if (!ifile.is_open())
		return Mat();

	int iRows = 0;
	int iCols = 0;
	int AType = -1;
	ifile.read( (char*)&iRows , sizeof(int) );
	ifile.read( (char*)&iCols , sizeof(int) );
	ifile.read( (char*)&AType , sizeof(int) );

	Mat kMatrix( iRows , iCols , AType );

	char* pbuff = (char*) kMatrix.data;
	ifile.read( pbuff, kMatrix.elemSize()*kMatrix.total());

	return kMatrix;
}

bool writemat( const string& filename, Mat origA )
{
	ofstream ofile;
	ofile.open( filename.c_str() , ios::binary );
	if( !ofile.is_open() )
		return false;

	bool tmp = writemat(ofile, origA);

	ofile.close();

	return tmp;
}

bool writemat( ofstream& ofile, Mat origA )
{
	Mat A;
	if (origA.isContinuous())
		A = origA;
	else
		A = origA.clone();

	assert(A.isContinuous());

	if( !ofile.is_open() )
		return false;

	int iRows = A.rows;
	int iCols = A.cols;
	int AType = A.type();

	ofile.write( (char*)&iRows , sizeof(int) );
	ofile.write( (char*)&iCols , sizeof(int) );
	ofile.write( (char*)&AType , sizeof(int) );

	char* pbuff = (char*) A.data;

	ofile.write(pbuff, A.elemSize()*A.total());

	return true;
}

bool coutmat( const string& filename, Mat A )
{
	ofstream ofile;
	ofile.open(filename.c_str());
	if (!ofile.is_open() || A.channels()!=1)
		return false;

	int iRows = A.rows;
	int iCols = A.cols;
	int AType = A.type();

	Mat B;
	A.convertTo(B, CV_64FC1);

	ofile << iRows << "\t" << iCols << "\t" << AType << endl;

	for (int i=0; i<iRows; i++)
	{
		for (int j=0; j<iCols; j++)
		{
			ofile << B.at<double>(i,j) << "\t";
		}
		ofile << endl;
	}
	return true;
}

bool writemat_for_matlab( const string& filename, Mat A )
{//A is MxN matrix. Its data is row-first. In matlab it should be column-first.
	Mat origB = A.t();
	Mat B;
	if (origB.isContinuous())
		B = origB;
	else
		B = origB.clone();

	ofstream ofile;
	ofile.open( filename.c_str() , ios::binary );
	if( !ofile.is_open() )
		return false;

	int iRows = A.rows;
	int iCols = A.cols;
	int AType = A.type();

	ofile.write( (char*)&iRows , sizeof(int) );
	ofile.write( (char*)&iCols , sizeof(int) );
	ofile.write( (char*)&AType , sizeof(int) );

	char* pbuff = (char*) B.data;

	ofile.write(pbuff, B.elemSize()*B.total());
	ofile.close();

	return true;

}

cv::Mat loadmat_from_matlab( const string& filename )
{
	ifstream ifile;
	ifile.open( filename.c_str() , ios::binary );

	if (!ifile.is_open())
		return Mat();

	int iRows = 0;
	int iCols = 0;
	int AType = -1;
	ifile.read( (char*)&iRows , sizeof(int) );
	ifile.read( (char*)&iCols , sizeof(int) );
	ifile.read( (char*)&AType , sizeof(int) );

	Mat kMatrix( iCols , iRows , AType );

	char* pbuff = (char*) kMatrix.data;
	ifile.read( pbuff, kMatrix.elemSize()*kMatrix.total());
	ifile.close();

	return kMatrix.t();
}

cv::Mat randmat( int row, int col )
{
	srand(time(NULL));
	int rangenum = 10000000;

	Mat A(row, col, CV_64FC1);
	double* ptr = (double*)A.data;

	for (int i=0; i<row*col; i++)
		ptr[i] = (rand() % rangenum) / (double)rangenum;

	return A;
}

cv::Mat seqmat( int row, int col )
{
	Mat A(row, col, CV_64FC1);
	double tmp = 1.0;
	for (int i=0; i<row; i++)
	{
		for (int j=0; j<col; j++)
		{
			A.at<double>(i,j) = tmp;
			tmp += 1.0;
		}
	}
	return A;
}

cv::Mat repmat( Mat A, int nrows, int ncols )
{
	Mat rst(nrows*A.rows, ncols*A.cols, A.type());
	Mat tmp;
	for (int i=0; i<nrows; i++)
	{
		for (int j=0; j<ncols; j++)
		{
			tmp = rst(Range(i*A.rows, (i+1)*A.rows), Range(j*A.cols, (j+1)*A.cols));
			A.copyTo(tmp);
		}
	}
	return rst;
}

cv::Mat max( Mat A, int dim )
{
	Mat B;
	double max_current_roworcolumn;
	switch (dim)
	{
	case 1:
		B = Mat(A.rows, 1, CV_64FC1);

		for (int i=0; i<A.rows; i++)
		{
			max_current_roworcolumn = -DBL_MAX;
			for (int j=0; j<A.cols; j++)
				max_current_roworcolumn = max((double)A.at<double>(i,j), (double)max_current_roworcolumn);
			B.at<double>(i,0) = max_current_roworcolumn;
		}
		break;
	case 2:
		B = Mat(1, A.cols, CV_64FC1);

		for (int j=0; j<A.cols; j++)
		{
			max_current_roworcolumn = -DBL_MAX;
			for (int i=0; i<A.rows; i++)
				max_current_roworcolumn = max((double)A.at<double>(i,j), (double)max_current_roworcolumn);
			B.at<double>(0,j) = max_current_roworcolumn;
		}
		break;
	default:
		pexit("max(Mat,int) only accept int==1 or 2");
	}
	return B;
}

cv::Mat sum( Mat A, int dim )
{
	Mat B;
	double sum_of_roworcolumn;
	switch (dim)
	{
	case 1:
		B = Mat(A.rows, 1, CV_64FC1);

		for (int i=0; i<A.rows; i++)
		{
			sum_of_roworcolumn = 0.0;
			for (int j=0; j<A.cols; j++)
				sum_of_roworcolumn += A.at<double>(i,j);
			B.at<double>(i,0) = sum_of_roworcolumn;
		}
		break;
	case 2:
		B = Mat(1, A.cols, CV_64FC1);

		for (int j=0; j<A.cols; j++)
		{
			sum_of_roworcolumn = 0.0;
			for (int i=0; i<A.rows; i++)
				sum_of_roworcolumn += A.at<double>(i,j);
			B.at<double>(0,j) = sum_of_roworcolumn;
		}
		break;
	default:
		pexit("sum(Mat,int) only accept int==1 or 2");
	}
	return B;
}

cv::Mat cvt_to_rgba( Mat img, bool is_transparent )
{
	Mat tmp;
	if (img.channels() == 1) // Gray image
		cvtColor(img, tmp, CV_GRAY2BGR);
	else
		if (img.channels() == 3)
			tmp = img;
		else
			return Mat();

	Mat alpha;
	if (is_transparent)
	{
		alpha = Mat::zeros(tmp.size(), CV_8UC1);
	}
	else
	{
		alpha = Mat(tmp.size(), CV_8UC1, Scalar(255));
	}

	Mat bgra;
	vector<Mat> vec_bgra(2);
	vec_bgra[0] = tmp;
	vec_bgra[1] = alpha;

	merge(vec_bgra, bgra);
	return bgra;
}

cv::Mat to_fixed_color( Mat img, Scalar color )
{
	Mat rst(img.size(), img.type(), Scalar::all(0));
	if (img.channels() == 3)
	{
		for (int i=0; i<img.rows; i++)
		{
			for (int j=0; j<img.cols; j++)
			{
				Vec3b tmp = img.at<Vec3b>(i,j);
				if (img.at<Vec3b>(i,j) != Vec3b(0,0,0))
				{
					rst.at<Vec3b>(i,j) = Vec3b((uchar)color[0], (uchar)color[1], (uchar)color[2]);
				}
			}
		}
	}
	else
		if (img.channels() == 1)
		{
			for (int i=0; i<img.rows; i++)
			{
				for (int j=0; j<img.cols; j++)
				{
					if (img.at<uchar>(i,j) != 0)
					{
						rst.at<uchar>(i,j) = color[0];
					}
				}
			}
		}
		else
			rst = Mat();

	return rst;
}

void writevideo( const vector<Mat>& video, const string& video_filename )
{
	int numframes = video.size();
	double framerate = 24.0;

	VideoWriter vw;

	vw.open(video_filename, CV_FOURCC('X','V','I','D'), framerate, video[0].size(), true);

	if (!vw.isOpened())
		pexit("No Xvid Codec Installed, Please Go to http://www.xvidmovies.com/codec/ to Download it!");

	for (int k=0; k<numframes; k++)
		vw << video[k];
}

vector<Mat> read_from_images( const string& foldername, const string& suffix )
{
	vector<string> filelist = show_file_list(foldername, suffix);
	int numframes = filelist.size();

	if (numframes == 0)
	{
		cout << "No frame in folder: " << foldername << endl;
		pexit("*********************************************");
	}

	vector<Mat> video(numframes);
	string filename;
	for (int k=0; k<numframes; k++)
	{
		filename = foldername + filelist[k];
		Mat raw_frame = imread(filename.c_str(), CV_LOAD_IMAGE_COLOR);
		video[k] = raw_frame;
		cout << ".";
	}
	cout << endl;

	return video;
}

cv::Mat LBP( Mat c3image )
{
	int iRows = c3image.rows;
	int iCols = c3image.cols;

	int aiMap[8][3] = {
		-1, -1, 1,
		-1, 0, 2,
		-1, 1, 4,
		0, 1, 8,
		1, 1, 16,
		1, 0, 32,
		1, -1, 64,
		0, -1, 128
	};

	Mat kImage;
	cvtColor( c3image , kImage , CV_RGB2GRAY );
	Mat lbpimage = Mat::zeros( kImage.size() , CV_8UC1 );
	for( int iRow = 1; iRow < iRows-1; iRow++ )
	{
		uchar* pkImageLine = kImage.ptr<uchar>( iRow );
		uchar* pkLBPLine = lbpimage.ptr<uchar>( iRow );
		for( int iCol = 1; iCol < iCols-1; iCol++ )
		{
			uchar ucCode = 0;
			uchar ucValue = pkImageLine[iCol];
			for( int k = 0 ; k < 8 ; k++ )
			{
				int iNRow = iRow + aiMap[k][0];
				int iNCol = iCol + aiMap[k][1];
				uchar ucNValue = kImage.at<uchar>( iNRow , iNCol );
				if( ucValue < ucNValue )
					ucCode += aiMap[k][2];
			}
			pkLBPLine[iCol] = ucCode;
		}
	}
	//handle boundaries
	for( int iRow = 1; iRow < iRows-1; iRow++ )
	{
		lbpimage.at<uchar>( iRow, 0 ) = lbpimage.at<uchar>( iRow, 1 );
		lbpimage.at<uchar>( iRow, iCols-1 ) = lbpimage.at<uchar>( iRow, iCols-2 );
	}
	for( int iCol = 0; iCol < iCols; iCol++ )
	{
		lbpimage.at<uchar>( 0, iCol ) = lbpimage.at<uchar>( 1, iCol );
		lbpimage.at<uchar>( iRows-1, iCol ) = lbpimage.at<uchar>( iRows-2, iCol );
	}

	return lbpimage;
}

cv::Mat splicemats( const vector<Mat>& mats, const string& flag )
{
	if (flag != "horizontal" && flag != "vertical")
		return Mat();

	Mat S;

	if (flag == "horizontal")
	{
		int totalcols = 0;
		for (int i=0; i<mats.size(); i++)
			totalcols += mats[i].cols;

		S = Mat(mats[0].rows, totalcols, mats[0].type());

		Mat tmp;
		int offset = 0;
		for (int i=0; i<mats.size(); i++)
		{
			tmp = S.colRange(offset, offset+mats[i].cols);
			mats[i].copyTo(tmp);
			offset += mats[i].cols;
		}
	}

	if (flag == "vertical")
	{
		int totalrows = 0;
		for (int i=0; i<mats.size(); i++)
			totalrows += mats[i].rows;

		S = Mat(totalrows, mats[0].cols, mats[0].type());

		Mat tmp;
		int offset = 0;
		for (int i=0; i<mats.size(); i++)
		{
			tmp = S.rowRange(offset, offset+mats[i].rows);
			mats[i].copyTo(tmp);
			offset += mats[i].rows;
		}
	}

	return S;		
}

void slitmat( vector<Mat>& mats, Mat bigmat, const vector<int>& nums, const string& flag )
{
	mats.resize(nums.size());
	int offset = 0;
	for (int i=0; i<nums.size(); i++)
	{
		Mat tmp;
		if (flag == "horizontal")
		{
			tmp = Mat(bigmat.rows, nums[i], bigmat.type(), Scalar(0.0));
			bigmat.colRange(offset, offset+nums[i]).copyTo(tmp);
		}
		else if (flag == "vertical")
		{
			tmp = Mat(nums[i], bigmat.cols, bigmat.type(), Scalar(0.0));
			bigmat.rowRange(offset, offset+nums[i]).copyTo(tmp);
		}
		else
		{
			pexit("Error: slitmat: flag, not support:" + flag);
		}

		mats[i] = tmp;
		offset += nums[i];
	}
}

bool write_sparse_matrix( const string& filename, const Sparse_Matrix* A )
{
	if (A->get_fill_state() == 0)
	{
		// Get all A's information
		int Arows = A->rows();
		int Acols = A->cols();
		SYMMETRIC_STATE A_sym = A->symmetric_state();// SYM_UPPER/SYM_LOWER/NOSYM
		bool Aisspd = A->isspd();
		SPARSE_STORAGE A_storage = A->storage();// CRS/CCS/TRIPLE

		vector< vector<Sparse_Entry> >& entryset = A->get_entryset();
		Mat ElemToStore;
		// ElemToStore:
		// 最后两行的数值为：行数，列数，对称性
		//					spd， storage, INF
		int num_elem_to_store = 0;

		if (A_storage == CRS)
		{
			for (int rowID=0; rowID<entryset.size(); rowID++)
				num_elem_to_store += entryset[rowID].size();

			ElemToStore = Mat(num_elem_to_store+2, 3, CV_64FC1, Scalar(-1));
			int count = 0;
			for (int rowID=0; rowID<entryset.size(); rowID++)
			{
				for (int j=0; j<entryset[rowID].size(); j++)
				{
					int colID = entryset[rowID][j].index;
					double value = entryset[rowID][j].value;

					ElemToStore.at<double>(count, 0) = (double)rowID;
					ElemToStore.at<double>(count, 1) = (double)colID;
					ElemToStore.at<double>(count, 2) = value;
					count++;
				}
			}
			ElemToStore.at<double>(num_elem_to_store, 0) = (double)Arows;
			ElemToStore.at<double>(num_elem_to_store, 1) = (double)Acols;
			ElemToStore.at<double>(num_elem_to_store, 2) = (double)A_sym;
			ElemToStore.at<double>(num_elem_to_store+1, 0) = (double)Aisspd;
			ElemToStore.at<double>(num_elem_to_store+1, 1) = (double)A_storage;
			ElemToStore.at<double>(num_elem_to_store+1, 2) = DBL_MAX;
		}
		if (A_storage == CCS)
		{
			for (int colID=0; colID<entryset.size(); colID++)
				num_elem_to_store += entryset[colID].size();

			ElemToStore = Mat(num_elem_to_store+2, 3, CV_64FC1, Scalar(-1));
			int count = 0;
			for (int colID=0; colID<entryset.size(); colID++)
			{
				for (int j=0; j<entryset[colID].size(); j++)
				{
					int rowID = entryset[colID][j].index;
					double value = entryset[colID][j].value;

					ElemToStore.at<double>(count, 0) = (double)rowID;
					ElemToStore.at<double>(count, 1) = (double)colID;
					ElemToStore.at<double>(count, 2) = value;
					count++;
				}
			}
			ElemToStore.at<double>(num_elem_to_store, 0) = (double)Arows;
			ElemToStore.at<double>(num_elem_to_store, 1) = (double)Acols;
			ElemToStore.at<double>(num_elem_to_store, 2) = (double)A_sym;
			ElemToStore.at<double>(num_elem_to_store+1, 0) = (double)Aisspd;
			ElemToStore.at<double>(num_elem_to_store+1, 1) = (double)A_storage;
			ElemToStore.at<double>(num_elem_to_store+1, 2) = DBL_MAX;
		}
		if (A_storage == TRIPLE)
		{
			pexit("Not implemented!");
		}

		return writemat(filename, ElemToStore);

	}
	else
		if (A->get_fill_state() == 1)
			pexit("Current Not Support: Write Sparse_Matrix::DISABLE");
		else
			pexit("Current Not Support: Write Sparse_Matrix::LOCK");
}

Sparse_Matrix* load_sparse_matrix( const string& filename )
{
	Mat ElemToStore = loadmat(filename);
	if (ElemToStore.empty())
		return NULL;

	int num_elem_to_store = ElemToStore.rows - 2;
	SYMMETRIC_STATE A_sym;
	SPARSE_STORAGE A_storage;

	int Arows = (int)(ElemToStore.at<double>(num_elem_to_store, 0));
	int Acols = (int)(ElemToStore.at<double>(num_elem_to_store, 1));

	int tmp1 = (int)(ElemToStore.at<double>(num_elem_to_store, 2));// SYM_UPPER/SYM_LOWER/NOSYM
	switch (tmp1)
	{
	case 0:
		A_sym = NOSYM; break;
	case 1:
		A_sym = SYM_UPPER; break;
	case 2:
		A_sym = SYM_LOWER; break;
	case 3:
		A_sym = SYM_BOTH; break;
	}

	bool Aisspd = (fabs(ElemToStore.at<double>(num_elem_to_store+1, 0)) > 1e-7);

	int tmp2 = (int)(ElemToStore.at<double>(num_elem_to_store+1, 1));
	switch (tmp2)
	{
	case 0:
		A_storage = CCS; break;
	case 1:
		A_storage = CRS; break;
	case 2:
		A_storage = TRIPLE; break;
	}

	Sparse_Matrix* A = new Sparse_Matrix(Arows, Acols, A_sym, Aisspd, A_storage);

	A->begin_fill_entry();
	for (int count=0; count<num_elem_to_store; count++)
	{
		int rowID = ElemToStore.at<double>(count, 0);
		int colID = ElemToStore.at<double>(count, 1);
		double value = ElemToStore.at<double>(count, 2);
		A->set_entry(rowID, colID, value);
	}

	return A;
}

Vec3b color_map( int index, int nBins ) 
{
	if (index < 0 || index >= nBins)
		return Vec3b(255,255,255);
	else
	{
		double tmp = (double)index / (double)nBins * 63.0;
		int whichrow_floor = (int) floor(tmp);
		int whichrow_ceil = (int) ceil(tmp);
		double sigma = tmp - whichrow_floor;
		Vec3b color = sigma*glbColorMapJet.at<Vec3b>(whichrow_ceil, 0) + (1.0-sigma)*glbColorMapJet.at<Vec3b>(whichrow_floor, 0);
		return color;
	}
}

cv::Mat WinnFilterBank( Mat OrigLabImg )
{
	//Img must be Lab color image
	//Mat LabImg;
	//cvtColor(Img, LabImg, CV_BGR2Lab);
	Mat LabImg;

	if (OrigLabImg.type() != CV_64FC3)
		OrigLabImg.convertTo(LabImg, CV_64FC3);
	else
		LabImg = OrigLabImg.clone();

	vector<Mat> Lab;
	split(LabImg, Lab);
	Mat L, a, b;
	L = Lab[0];
	a = Lab[1];
	b = Lab[2];

	vector<Mat> features(17);

	for (int channel=0; channel<3; channel++)
	{
		for (int j=0; j<3; j++)
		{
			double sigma = pow(2.0, j);
			int sizexy = (int)(2.0*floor(3.0*sigma/2.0) + 1.0);
			Mat feature;
			GaussianBlur(Lab[channel], feature, Size(sizexy, sizexy), sigma);
			//feature.convertTo(feature, CV_64FC1);
			features[3*channel + j] = feature;
		}
	}

	for (int i=0; i<4; i++)
	{
		double sigma = pow(2.0, i);
		int width = (int)(2.0*floor(3.0*sigma/2.0) + 1.0);
		Mat kernel(width, width, CV_64FC1, Scalar(0.0));
		for (int y=0; y<width; y++)
		{
			for (int x=0; x<width; x++)
			{
				double r2 = (y - width/2)*(y - width/2) + (x - width/2)*(x - width/2);
				kernel.at<double>(y,x) = (r2 - pow(sigma, -2.0)) * exp(-r2 / 2.0 / sigma);
			}
		}
		kernel /= sum(kernel)[0];
		Mat feature;
		filter2D(L, feature, -1, kernel);
		//feature.convertTo(feature, CV_64FC1);
		features[9+i] = feature;
	}

	for (int i=0; i<2; i++)
	{
		double sigma = pow(2.0, i);
		double constant = 1.0 / (2.0*CV_PI*sqrt(sigma));
		int width = (int)(2.0*floor(5.0*sigma/2.0) + 1.0);
		Mat kernel1(width, width, CV_64FC1, Scalar(0.0));
		Mat kernel2(width, width, CV_64FC1, Scalar(0.0));
		for (int y=0; y<width; y++)
		{
			for (int x=0; x<width; x++)
			{
				double r2 = (y - width/2)*(y - width/2) + (x - width/2)*(x - width/2);
				kernel1.at<double>(y,x) = constant * exp(-r2 / 2.0 / sigma / sigma) * 
					(double)(x - width/2) / sigma / sigma;
				kernel2.at<double>(y,x) = constant * exp(-r2 / 2.0 / sigma / sigma) * 
					(double)(y - width/2) / sigma / sigma;
			}
		}
		Mat feature1, feature2;
		filter2D(L, feature1, -1, kernel1);
		filter2D(L, feature2, -1, kernel2);
		//feature1.convertTo(feature1, CV_64FC1);
		//feature2.convertTo(feature2, CV_64FC1);
		features[2*i+13] = feature1;
		features[2*i+14] = feature2;
	}

	Mat Feature17D(LabImg.size(), CV_64FC(17));
	merge(features, Feature17D);
	return Feature17D;
}

cv::Mat closest_rank1( const Mat& H, const Mat& origH )
{
	if (H.cols < 2)
		return H;

	SVD mSVD(H, SVD::FULL_UV);
	Mat S, u, vT;
	mSVD.compute(H, S, u, vT);

	Mat P;
	Mat u0, S0, vT0;
	u0 = u.col(0);
	S0 = S.row(0);
	vT0 = vT.row(0);
	P = u0*S0*vT0;

	Mat diff = origH - P;
	vector<int> whichrowlesszero;
	for (int i=0; i<diff.rows; i++)
	{
		for (int j=0; j<diff.cols; j++)
		{
			if (diff.at<double>(i,j) < 1.0e-7)
			{
				whichrowlesszero.push_back(i);
				break;
			}
		}
	}

	if (whichrowlesszero.size() > 0)
	{
		Mat sign_u0(u0.size(), CV_64FC1, Scalar(0.0));
		for (int i=0; i<u0.rows; i++)
		{
			if (u0.at<double>(i,0) > 1.0e-7)
				sign_u0.at<double>(i,0) = 1.0;
			else
				if (u0.at<double>(i,0) < -1.0e-7)
					sign_u0.at<double>(i,0) = -1.0;
		}

		Mat Ha(whichrowlesszero.size(), H.cols, CV_64FC1, Scalar(0.0));
		for (int i=0; i<whichrowlesszero.size(); i++)
			H.row(whichrowlesszero[i]).copyTo(Ha.row(i));

		Mat S0_vT0 = S0*vT0;
		Mat Tmp = repeat(S0_vT0, whichrowlesszero.size(), 1);
		Mat min_v = abs(Ha / Tmp);
		Mat Tmp2(min_v.rows, 1, CV_64FC1, Scalar(0.0));
		for (int i=0; i<min_v.rows; i++)
		{
			double tmp = min_v.at<double>(i,0);
			for (int j=1; j<min_v.cols; j++)
				tmp = min_v.at<double>(i,j) < tmp ? min_v.at<double>(i,j) : tmp;

			Tmp2.at<double>(i,0) = tmp;
		}

		Mat sign_u0a(whichrowlesszero.size(), 1, CV_64FC1, Scalar(0.0));
		for (int i=0; i<whichrowlesszero.size(); i++)
			sign_u0.row(whichrowlesszero[i]).copyTo(sign_u0a.row(i));

		Mat Tmp3 = sign_u0a.mul(Tmp2);
		for (int i=0; i<whichrowlesszero.size(); i++)
			Tmp3.row(i).copyTo(u0.row(whichrowlesszero[i]));

		P = u0*S0*vT0;
	}
	return P;
}
