#include "../RobustMatting/RobustMatting.h"
#include "cwgLibs.h"
#include "cwgUtils.h"

void example()
{
	Mat rgbimg = imread("../data/dog800x531.png");
	Mat trimap = imread("../data/trimap800x531.png");

	cwg::RobustMatting rm(rgbimg, trimap);

	rm.RunMatting();

	Mat rstimg = rm.GetFinalResult();
	imwrite("../data/rst.png", rstimg);
}

Mat refine_seg(Mat rawseg, Mat rgbimg)
{
	int radius = 13;
	Mat tmp = getStructuringElement(MORPH_ELLIPSE, Size(radius,radius));

	Mat erode_rawseg;
	Mat dilate_rawseg;
	erode(rawseg, erode_rawseg, tmp);
	dilate(rawseg, dilate_rawseg, tmp);
	dilate_rawseg = dilate_rawseg - erode_rawseg;

	Mat trimap;
	cvtColor(erode_rawseg, trimap, CV_GRAY2BGR);
	for (int i=0; i<trimap.rows; i++)
	{
		for (int j=0; j<trimap.cols; j++)
		{
			if (dilate_rawseg.at<uchar>(i,j))
				trimap.at<Vec3b>(i,j) = Vec3b(0,0,255);
		}
	}

	cwg::RobustMatting rm(rgbimg, trimap, 5.0);

	rm.RunMatting();

	return rm.GetFinalResult();
}

void batchrun()
{
	string foldername_raw = "D:\\newexp_for_tmm\\toybuzzy\\both\\final\\v1-raw\\";
	string foldername_mask = "D:\\newexp_for_tmm\\toybuzzy\\both\\final\\v1-mask\\";
	string foldername_refine = "D:\\newexp_for_tmm\\toybuzzy\\both\\final\\v1-refine-mask\\";
	mkdir(foldername_refine.c_str());
	vector<string> filelist_raw = show_file_list(foldername_raw, "jpg");
	vector<string> filelist_mask = show_file_list(foldername_mask, "png");

	char str[300];
	for (int i=0; i<filelist_raw.size(); i++)
	{
		string file_raw = filelist_raw[i];
		string file_mask = filelist_mask[i];
		Mat rgbimg = imread(foldername_raw+file_raw, CV_LOAD_IMAGE_COLOR);
		Mat raw_seg = imread(foldername_mask+file_mask, CV_LOAD_IMAGE_GRAYSCALE);
		Mat rst_mask = refine_seg(raw_seg, rgbimg);
		sprintf(str, "refine_%03d.png", i);
		string file_refine = foldername_refine + string(str);
		imwrite(file_refine, rst_mask);
	}
}

void example2( int argc, char** argv );

void main(int argc, char** argv)
{
	string foldername_raw = "D:\\newexp_for_tmm\\blackcar\\both\\final\\v0-raw\\";
	string foldername_mask = "D:\\newexp_for_tmm\\blackcar\\both\\final\\v0-mask\\";
	string foldername_refine = "D:\\newexp_for_tmm\\blackcar\\both\\final\\v0-refine-mask\\";
	mkdir(foldername_refine.c_str());
	vector<string> filelist_raw = show_file_list(foldername_raw, "jpg");
	vector<string> filelist_mask = show_file_list(foldername_mask, "png");

	char str[300];
	for (int i=0; i<filelist_raw.size(); i++)
	{
		string file_raw = filelist_raw[i];
		string file_mask = filelist_mask[i];
		Mat rgbimg = imread(foldername_raw+file_raw, CV_LOAD_IMAGE_COLOR);
		Mat raw_seg = imread(foldername_mask+file_mask, CV_LOAD_IMAGE_GRAYSCALE);
		Mat rst_mask = refine_seg(raw_seg, rgbimg);

		threshold(rst_mask, rst_mask, 127, 255, THRESH_BINARY);

		sprintf(str, "refine_%03d.png", i);
		string file_refine = foldername_refine + string(str);
		imwrite(file_refine, rst_mask);
	}
}

void example2( int argc, char** argv )
{
	if (argc != 4)
	{
		cout << "exe src_rgb_img trimap fineness(1.0)" << endl;
		system("pause");
		exit(0);
	}

	Mat rgbimg = imread(argv[1]);
	Mat trimap = imread(argv[2]);
	double fineness = atof(argv[3]);

	cwg::RobustMatting rm(rgbimg, trimap, fineness);

	Timer timer_obj;
	timer_obj.start_timer();
	rm.RunMatting();
	timer_obj.stop_timer();
	double sec = timer_obj.get_time();

	Mat rstimg = rm.GetFinalResult();

	char str[300];
	sprintf(str, "rst-%4.7lfsec.png", sec);
	imwrite(str, rstimg);
}
