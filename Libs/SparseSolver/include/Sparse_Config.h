#ifndef SPARSE_CONFIG_H
#define SPARSE_CONFIG_H


//	Special version for Geometry4Architecture

/** \defgroup MathSuite MathSuite: Sparse Matrix, Solver, QPSolver */

//! BLAS and LAPACK
/*!
*	<A HREF="http://www.tacc.utexas.edu/resources/software/"> GOTOBLAS 1.19 </A>
*	<A HREF="http://www.netlib.org/lapack/"> LAPACK 3.1.1 </A> 
*/
//////////////////////////////////////////////////////////////////////////
//#pragma comment (lib, "libgoto_northwood-r1.19.lib") 
//#pragma comment (lib, "libgoto_core2-r1.19.lib") 
//#pragma comment (lib, "blas_win32.lib") 
//#pragma comment (lib, "lapack_win32.lib") // commment by Sky

//#define USE_TAUCS
#define USE_ARPACK
//#define USE_UMFPACK


#endif //SPARSE_CONFIG_H
