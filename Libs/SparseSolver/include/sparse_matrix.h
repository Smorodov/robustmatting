#ifndef SPARSE_MATRIX_H
#define SPARSE_MATRIX_H

#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <ostream>
#include <cmath>

#ifndef WIN32
	#include <string.h>  // for memset
#endif // WIN32

#include "Sparse_Entry.h"

#pragma warning(disable:981)


// \addtogroup MathSuite 
//@{

//! Symmetric status 
enum SYMMETRIC_STATE
{
	NOSYM, /*!< general case */ 
	SYM_UPPER, /*!< symmetric (store upper triangular part) */
	SYM_LOWER, /*!< symmetric (store lower triangular part) */
	SYM_BOTH /*!< symmetric (store both upper and lower triangular part) */	
};

//!	Storage way
enum SPARSE_STORAGE
{
	CCS, /*!< compress column format */
	CRS, /*!< compress row format */
	TRIPLE /*!< row-wise coordinate format */
};

//! C or Fortran
enum ARRAYTYPE
{
	FORTRAN_TYPE, C_TYPE
};

//////////////////////////////////////////////////////////////////////////
//! Lite Sparse Matrix Class
class Lite_Sparse_Matrix
{
private:

	//!	Status for creating sparse solver
	enum STORE_STATE
	{
		ENABLE, 
		DISABLE, 
		LOCK	 
	};

	STORE_STATE state_fill_entry;
	SYMMETRIC_STATE sym_state; 
	SPARSE_STORAGE s_store;
	ARRAYTYPE arraytype;

	int nrows; //!< number of rows
	int ncols; //!< number of columns
	int nonzero; //!< number of nonzeros
	//! pointers to where columns begin in rowind and values 0-based, length is (col+1) 
	/*!
	* When s_store is CRS, colptr stores column indices;
	*/
	int* colptr; 
	//! row indices, 0-based
	/*!
	* When s_store is CRS, rowind stores row-pointers
	*/
	int* rowind; 
	double* values; //!< nonzero values of the sparse matrix

	std::vector< std::vector<Sparse_Entry> > entryset; //!< store temporary sparse entries

public:

	//! Sparse matrix constructor
	/*!
	* \param m row dimension
	* \param n column dimension
	* \param symmetric_state 
	* \param m_store the storage format
	* \param atype Fortran or C type of array
	*/
	Lite_Sparse_Matrix(int m, int n, SYMMETRIC_STATE symmetric_state = NOSYM, SPARSE_STORAGE m_store = CCS, ARRAYTYPE atype = C_TYPE)
		:nrows(m), ncols(n), sym_state(symmetric_state), s_store(m_store), arraytype(atype),
		nonzero(0), colptr(NULL), rowind(NULL), values(NULL), state_fill_entry(DISABLE)
	{
		if (m != n)
		{
			symmetric_state = NOSYM;
		}

		int nn = (m_store == CCS? ncols:nrows);
		entryset.resize(nn);
	}

	//! Sparse matrix destructor
	~Lite_Sparse_Matrix()
	{
		clear_mem();
	}
	//! Start to build sparse matrix pattern 
	inline void begin_fill_entry()
	{
		state_fill_entry = ENABLE;
	}
	//! Construct sparse pattern
	void end_fill_entry()
	{
		assert(state_fill_entry==ENABLE);

		clear_mem();

		state_fill_entry = LOCK;

		int inc =  (arraytype==FORTRAN_TYPE?1:0);

		if (s_store == CCS)
		{
			//construct map and ccs matrix
			int i, j, k = 0;
			colptr = new int[ncols+1];
			colptr[0] = inc;
			for (j=1; j<ncols+1; j++) {
				colptr[j] = (int)entryset[j-1].size() + colptr[j-1];
			}

			nonzero = colptr[ncols];

			if (nonzero > 0)
			{
				rowind = new int[nonzero];
				values = new double[nonzero];

				for ( j=0; j<ncols; j++) {
					for ( i=0; i<colptr[j+1]-colptr[j]; i++) {
						rowind[k] = entryset[j][i].index+inc;
						values[k] = entryset[j][i].value;
						k++;
					}
				}

			}

		}
		else if (s_store == CRS)
		{
			//construct map and crs matrix
			int i, j, k = 0;
			rowind = new int[nrows+1];
			rowind[0] = inc;
			for (j=1; j<nrows+1; j++) {
				rowind[j] = (int)entryset[j-1].size() + rowind[j-1];
			}
			nonzero = rowind[nrows];
			if (nonzero > 0)
			{
				colptr = new int[nonzero];
				values = new double[nonzero];

				for ( j = 0; j<nrows; j++) {
					for ( i = 0; i<rowind[j+1]-rowind[j]; i++) {
						colptr[k] = entryset[j][i].index+inc;
						values[k] = entryset[j][i].value;
						k++;
					}
				}
			}
		}
		else if (s_store == TRIPLE)
		{
			int i, j, k = 0;
			nonzero = 0;
			for (i = 0; i < nrows; i++)
			{
				nonzero += (int)entryset[i].size();
			}

			if (nonzero > 0)
			{
				rowind = new int[nonzero];
				colptr = new int[nonzero];
				values = new double[nonzero];

				for (i = 0; i < nrows; i++)
				{
					int jsize = (int)entryset[i].size();
					for (j = 0; j < jsize; j++)
					{
						rowind[k] = i+inc;
						colptr[k] = entryset[i][j].index+inc;
						values[k] = entryset[i][j].value;
						k++;
					}
				}
			}
		}
		entryset.clear();
	}

	//! Fill matrix entry \f$ 	Mat[rowind, colind] += val \f$
	void fill_entry(int row_index, int col_index, double val = 0 )
	{
		if(sym_state == NOSYM)
		{
			fill_entry_internal(row_index, col_index, val);
		}
		else if (sym_state == SYM_UPPER)
		{
			if (row_index <= col_index)
			{
				fill_entry_internal(row_index, col_index, val);
			}
			else
			{
				fill_entry_internal(col_index, row_index, val);
			}
		}
		else if (sym_state == SYM_LOWER)
		{
			if (row_index <= col_index)
			{
				fill_entry_internal(col_index, row_index, val);
			}
			else
			{
				fill_entry_internal(row_index, col_index, val);
			}
		}
		else if (sym_state == SYM_BOTH)
		{
			fill_entry_internal(row_index, col_index, val);

			if (row_index != col_index)
			{
				fill_entry_internal(col_index, row_index, val);
			}
		}
	}

	//! get the number of nonzeros
	inline int get_nonzero() const
	{
		return nonzero;
	}
	//! get the row dimension
	inline int rows() const
	{
		return nrows;
	}
	//! get the column dimension
	inline int cols() const
	{
		return ncols;
	}
	//! return the symmetric state
	inline bool issymmetric() const
	{
		return sym_state != NOSYM;
	}

	//! tell whether the matrix is upper or lower symmetric
	inline bool issym_store_upper_or_lower() const
	{
		return (sym_state == SYM_LOWER) || (sym_state == SYM_UPPER);
	}

	//! return symmetric state
	inline SYMMETRIC_STATE symmetric_state() const
	{
		return sym_state;
	}

	//! tell whether the matrix is square
	inline bool issquare() const
	{
		return nrows == ncols;
	}

	//! return the storage format
	inline SPARSE_STORAGE storage() const
	{
		return s_store;
	}

	//! return array type
	inline ARRAYTYPE get_arraytype() const
	{
		return arraytype;
	}

	//! get rowind
	inline int* get_rowind() const
	{
		return rowind;
	}

	//! get colptr
	inline int* get_colptr() const
	{
		return colptr;
	}

	//! get the values array
	inline double* get_values() const
	{
		return values;
	}

	//////////////////////////////////////////////////////////////////////////
private:
	//! Clear memory
	void clear_mem()
	{
		if (colptr != NULL) 
		{
			delete[] colptr;
			colptr = NULL;
		}

		if (rowind !=NULL) 
		{
			delete[] rowind;
			rowind = NULL;
		}

		if (values !=NULL) 
		{
			delete[] values;
			values = NULL;
		}
	}

	//! fill matrix entry (internal) \f$ Mat[rowid][colid] += val \f$
	bool fill_entry_internal(int row_index, int col_index, double val = 0)
	{
		assert(state_fill_entry==ENABLE);

		int search_index = (s_store==CCS?row_index:col_index);
		int pos_index = (s_store==CCS?col_index:row_index);

		Sparse_Entry forcompare(search_index);

		std::vector<Sparse_Entry>::iterator iter =
			std::lower_bound(entryset[pos_index].begin(), entryset[pos_index].end(), forcompare);
		if (iter != entryset[pos_index].end())
		{
			if (iter->index == search_index)
			{
				iter->value += val;
			}
			else
				entryset[pos_index].insert(iter, Sparse_Entry(search_index, val));
		}
		else
		{
			entryset[pos_index].push_back(Sparse_Entry(search_index, val));
		}
		return true;
	}
	//////////////////////////////////////////////////////////////////////////
};

/////////////////////////////////////////////////////////////////////////
//! Sparse Matrix Class
class Sparse_Matrix
{
private:

	//!	Status for creating sparse solver
	enum STORE_STATE
	{
		ENABLE, 
		DISABLE, 
		LOCK	 
	};

	STORE_STATE state_fill_entry;
	SYMMETRIC_STATE sym_state; 
	SPARSE_STORAGE s_store;

	bool spd; //!< positive definite. only useful when the user call TAUCS solver

	int nrows; //!< number of rows
	int ncols; //!< number of columns
	int nonzero; //!< number of nonzeros
	//! pointers to where columns begin in rowind and values 0-based, length is (col+1) 
	/*!
	* When s_store is CRS, colptr stores column indices;
	*/
	int* colptr; 
	//! row indices, 0-based
	/*!
	* When s_store is CRS, rowind stores row-pointers
	*/
	int* rowind; 
	double* values; //!< nonzero values of the sparse matrix
	int m_num_column_of_RHS; //!< number of columns of right hide side


	std::vector< std::vector<Sparse_Entry> > entryset; //!< store temporary sparse entries
	std::map< std::pair<int, int>, int > sparseindex; //!< indices for quering the values fastly

	double *B; //!< Ax = B, store the right hand side directly
	double *solution; //!< the array for storing the solution

public:

	//! Sparse matrix constructor
	/*!
	* \param m row dimension
	* \param n column dimension
	* \param symmetric_state 
	* \param _spd whether the matrix is symmetric positive definite
	* \param m_store specify the storage format
	* \param num_column_of_RHS number of column of right hide
	*/
	Sparse_Matrix(int m, int n, SYMMETRIC_STATE symmetric_state = NOSYM, bool _spd = false, 
		SPARSE_STORAGE m_store = CCS, int num_column_of_RHS = 1)
		:nrows(m), ncols(n), sym_state(symmetric_state), spd(_spd), s_store(m_store), 
		m_num_column_of_RHS(num_column_of_RHS), 
		nonzero(0), colptr(NULL), rowind(NULL), values(NULL), state_fill_entry(DISABLE)
	{
		if (m != n)
		{
			symmetric_state = NOSYM;
			spd = false;
		}

		if (sym_state == NOSYM)
		{
			spd = false;
		}

		int nn = (m_store == CCS? ncols:nrows);
		entryset.resize(nn);

		B = new double[m_num_column_of_RHS*nrows];
		memset(B, 0, sizeof(double)*m_num_column_of_RHS*nrows);
		solution = new double[m_num_column_of_RHS*ncols];
		memset(solution, 0, sizeof(double)*m_num_column_of_RHS*ncols);
	}

	//! Sparse matrix destructor
	~Sparse_Matrix()
	{
		clear_mem();

		if (B != NULL) 
		{
			delete[] B;
			B = NULL;
		}

		if (solution!=NULL) 
		{
			delete[] solution;
			solution = NULL;
		}
	}
	//! Start to build sparse matrix pattern 
	inline void begin_fill_entry()
	{
		state_fill_entry = ENABLE;
	}
	//! Construct sparse pattern
	void end_fill_entry()
	{
		assert(state_fill_entry==ENABLE);

		clear_mem();
		sparseindex.clear();

		state_fill_entry = LOCK;

		if (s_store == CCS)
		{
			//construct map and ccs matrix
			int i, j, k = 0;
			colptr = new int[ncols+1];
			colptr[0] = 0;
			for (j=1; j<ncols+1; j++) {
				colptr[j] = (int)entryset[j-1].size() + colptr[j-1];
			}

			nonzero = colptr[ncols];

			if (nonzero > 0)
			{
				rowind = new int[nonzero];
				values = new double[nonzero];

				for ( j=0; j<ncols; j++) {
					for ( i=0; i<colptr[j+1]-colptr[j]; i++) {
						rowind[k] = entryset[j][i].index;
						values[k] = entryset[j][i].value;
						sparseindex[std::pair<int, int>(rowind[k], j)] = k+1;
						k++;
					}
				}

			}

		}
		else if (s_store == CRS)
		{
			//construct map and crs matrix
			int i, j, k = 0;
			rowind = new int[nrows+1];
			rowind[0] = 0;
			for (j=1; j<nrows+1; j++) {
				rowind[j] = (int)entryset[j-1].size() + rowind[j-1];
			}
			nonzero = rowind[nrows];
			if (nonzero > 0)
			{
				colptr = new int[nonzero];
				values = new double[nonzero];

				for ( j = 0; j<nrows; j++) {
					for ( i = 0; i<rowind[j+1]-rowind[j]; i++) {
						colptr[k] = entryset[j][i].index;
						values[k] = entryset[j][i].value;
						sparseindex[std::pair<int, int>(colptr[k], j)] = k+1;
						k++;
					}
				}
			}
		}
		else if (s_store == TRIPLE)
		{
			int i, j, k = 0;
			nonzero = 0;
			for (i = 0; i < nrows; i++)
			{
				nonzero += (int)entryset[i].size();
			}

			if (nonzero > 0)
			{
				rowind = new int[nonzero];
				colptr = new int[nonzero];
				values = new double[nonzero];

				for (i = 0; i < nrows; i++)
				{
					int jsize = (int)entryset[i].size();
					for (j = 0; j < jsize; j++)
					{
						rowind[k] = i;
						colptr[k] = entryset[i][j].index;
						values[k] = entryset[i][j].value;
						sparseindex[std::pair<int, int>(i, colptr[k])] = k+1;
						k++;
					}
				}
			}

		}

		entryset.clear();
	}


	//! Fill matrix entry \f$ 	Mat[rowind, colind] += val \f$
	void fill_entry(int row_index, int col_index, double val = 0 )
	{
		if(sym_state == NOSYM)
		{
			fill_entry_internal(row_index, col_index, val);
		}
		else if (sym_state == SYM_UPPER)
		{
			if (row_index <= col_index)
			{
				fill_entry_internal(row_index, col_index, val);
			}
			else
			{
				fill_entry_internal(col_index, row_index, val);
			}
		}
		else if (sym_state == SYM_LOWER)
		{
			if (row_index <= col_index)
			{
				fill_entry_internal(col_index, row_index, val);
			}
			else
			{
				fill_entry_internal(row_index, col_index, val);
			}
		}
		else if (sym_state == SYM_BOTH)
		{
			fill_entry_internal(row_index, col_index, val);

			if (row_index != col_index)
			{
				fill_entry_internal(col_index, row_index, val);
			}
		}
	}

	//! Get matrix entry \f$ Mat[rowid][colid] \f$
	double get_entry(int row_index, int col_index)
	{
		if (state_fill_entry==ENABLE)
		{
			int search_index = (s_store==CCS?row_index:col_index);
			int pos_index = (s_store==CCS?col_index:row_index);
			Sparse_Entry forcompare(search_index);

			std::vector<Sparse_Entry>::iterator iter =
				std::lower_bound(entryset[pos_index].begin(), entryset[pos_index].end(), forcompare);
			if (iter != entryset[pos_index].end())
			{
				if (iter->index == search_index)
				{
					return iter->value;
				}
			}
		}
		else
		{
			int id = -1;
			if (sym_state == NOSYM || sym_state == SYM_BOTH)
			{
				id = sparseindex[std::pair<int, int>(row_index, col_index)] - 1;
			}
			else if (sym_state == SYM_UPPER)
			{
				if (row_index <= col_index)
				{
					id = sparseindex[std::pair<int, int>(row_index, col_index)] - 1;
				}
				else
				{
					id = sparseindex[std::pair<int, int>(col_index, row_index)] - 1;
				}
			}
			else if (sym_state == SYM_LOWER)
			{
				if (row_index > col_index)
				{
					id = sparseindex[std::pair<int, int>(row_index, col_index)] - 1;
				}
				else
				{
					id = sparseindex[std::pair<int, int>(col_index, row_index)] - 1;
				}
			}

			if (id >= 0)
			{
				return values[id];
			}
			else
			{
				if((sym_state == SYM_UPPER && row_index > col_index) || 
					(sym_state == SYM_LOWER && row_index < col_index))
					erase_id(col_index, row_index);
				else
					erase_id(row_index, col_index);
				;

				return 0;
			}
		}

		return 0;
	}
	//! Set matrix entry \f$ Mat[rowid][colid] = val \f$
	void set_entry(int row_index, int col_index, double val = 0)
	{
		if(sym_state == NOSYM)
		{
			set_entry_internal(row_index, col_index, val);
		}
		else if (sym_state == SYM_UPPER)
		{
			if (row_index <= col_index)
			{
				set_entry_internal(row_index, col_index, val);
			}
			else
			{
				set_entry_internal(col_index, row_index, val);
			}
		}
		else if (sym_state == SYM_LOWER)
		{
			if (row_index <= col_index)
			{
				set_entry_internal(col_index, row_index, val);
			}
			else
			{
				set_entry_internal(row_index, col_index, val);
			}
		}
		else if (sym_state == SYM_BOTH)
		{
			set_entry_internal(row_index, col_index, val);

			if (row_index != col_index)
			{
				set_entry_internal(col_index, row_index, val);
			}
		}
	}

	std::vector< std::vector<Sparse_Entry> > get_entryset() const
	{//����
		return entryset;
	}

	const std::vector< std::vector<Sparse_Entry> >* get_entryset_pointer()
	{
		return &entryset;
	}

	inline STORE_STATE get_fill_state() const{return state_fill_entry;}

	//! Fill the right hand matrix entry \f$ B[rowid] += val \f$
	void fill_B_entry(int row_index, double val)
	{
		B[row_index] += val;
	}

	//! set the right hand matrix entry \f$ B[rowid] = val \f$
	void set_B_entry(int row_index, double val)
	{
		B[row_index] = val;
	}

	//! reset all values to zero
	bool reset2zero()
	{
		assert(state_fill_entry == LOCK);
		if (values)
		{
			memset(values, 0, sizeof(double)*m_num_column_of_RHS*nonzero);
		}
		if (B)
		{
			memset(B, 0, sizeof(double)*m_num_column_of_RHS*nrows);
		}
		return true;
	}
	//! get the solution array
	inline double* get_solution() const
	{
		return solution;
	}
	//! get the number of nonzeros
	inline int get_nonzero() const
	{
		return nonzero;
	}
	//! get the row dimension
	inline int rows() const
	{
		return nrows;
	}
	//! get the column dimension
	inline int cols() const
	{
		return ncols;
	}
	//! return the symmetric state
	inline bool issymmetric() const
	{
		return sym_state != NOSYM;
	}

	//! tell whether the matrix is upper or lower symmetric
	inline bool issym_store_upper_or_lower() const
	{
		return (sym_state == SYM_LOWER) || (sym_state == SYM_UPPER);
	}

	//! return symmetric state
	inline SYMMETRIC_STATE symmetric_state() const
	{
		return sym_state;
	}

	//! tell whether the matrix is SPD
	inline bool isspd() const
	{
		return spd;
	}

	//! tell whether the matrix is square
	inline bool issquare() const
	{
		return nrows == ncols;
	}

	//! return the storage format
	inline SPARSE_STORAGE storage() const
	{
		return s_store;
	}

	//! return the number column of right hand
	inline int get_num_rhs() const
	{
		return m_num_column_of_RHS;
	}

	//! get rowind
	inline int*& get_rowind()
	{
		return rowind;
	}

	//! get colptr
	inline int*& get_colptr()
	{
		return colptr;
	}

	//! get the values array
	inline double*& get_values()
	{
		return values;
	}

	//! get rowind
	inline int* get_rowind() const
	{
		return rowind;
	}

	//! get colptr
	inline int* get_colptr() const
	{
		return colptr;
	}

	//! get the values array
	inline double* get_values() const
	{
		return values;
	}

	//! get the right hand side
	inline double* get_B() const
	{
		return B;
	}
	//////////////////////////////////////////////////////////////////////////
private:
	//! Clear memory
	void clear_mem()
	{
		if (colptr != NULL) 
		{
			delete[] colptr;
			colptr = NULL;
		}

		if (rowind !=NULL) 
		{
			delete[] rowind;
			rowind = NULL;
		}

		if (values !=NULL) 
		{
			delete[] values;
			values = NULL;
		}
	}

	//! fill matrix entry (internal) \f$ Mat[rowid][colid] += val \f$
	bool fill_entry_internal(int row_index, int col_index, double val = 0)
	{
		if (state_fill_entry==ENABLE)
		{
			int search_index = (s_store==CCS?row_index:col_index);
			int pos_index = (s_store==CCS?col_index:row_index);

			Sparse_Entry forcompare(search_index);

			std::vector<Sparse_Entry>::iterator iter =
				std::lower_bound(entryset[pos_index].begin(), entryset[pos_index].end(), forcompare);
			if (iter != entryset[pos_index].end())
			{
				if (iter->index == search_index)
				{
					iter->value += val;
				}
				else
					entryset[pos_index].insert(iter, Sparse_Entry(search_index, val));
			}
			else
			{
				entryset[pos_index].push_back(Sparse_Entry(search_index, val));
			}
		}
		else
		{
			int id = sparseindex[std::pair<int, int>(row_index, col_index)] - 1;
			if (id >= 0)
			{
				values[id] += val;
			}
			else
			{
				erase_id(row_index, col_index);
				return false;
			}
		}

		return true;
	}
	//! set matrix entry (internal) \f$ Mat[rowid][colid] = val \f$
	bool set_entry_internal(int row_index, int col_index, double val = 0)
	{
		if (state_fill_entry==ENABLE)
		{
			int search_index = (s_store==CCS?row_index:col_index);
			int pos_index = (s_store==CCS?col_index:row_index);

			Sparse_Entry forcompare(search_index);

			std::vector<Sparse_Entry>::iterator iter =
				std::lower_bound(entryset[pos_index].begin(), entryset[pos_index].end(), forcompare);
			if (iter != entryset[pos_index].end())
			{
				if (iter->index == search_index)
				{
					iter->value = val;
				}
				else
					entryset[pos_index].insert(iter, Sparse_Entry(search_index, val));
			}
			else
			{
				entryset[pos_index].push_back(Sparse_Entry(search_index, val));
			}
		}
		else
		{
			int id = sparseindex[std::pair<int, int>(row_index, col_index)] - 1;
			if (id >= 0)
			{
				values[id] = val;
			}
			else
			{
				erase_id(row_index, col_index);
				return false;
			}
		}

		return true;
	}
	//////////////////////////////////////////////////////////////////////////
	void erase_id(int id_i , int id_j)
	{
		std::map<std::pair<int, int>, int>::iterator miter = sparseindex.find(std::pair<int, int>(id_i, id_j));
		sparseindex.erase(miter);
	}
	//////////////////////////////////////////////////////////////////////////
};

//! print sparse matrix
std::ostream& operator<<(std::ostream &s, const Sparse_Matrix *A);

// !print the spare matrix to a CFile. Avoid using standard output stream
// void output_sparse_matrix( CFile* file, Sparse_Matrix* A  );
// void output_sparse_matrix_nonzeroes( CFile* file, Sparse_Matrix* A );
void output_sparse_matrix_all_entries( std::string str_file_name, Sparse_Matrix* A );

void export_for_superlu_dist( std::string str_file_name, Sparse_Matrix* A );
Sparse_Matrix* import_matrix_for_superlu_dist( std::string str_file_name );

Sparse_Matrix* get_copy( const Sparse_Matrix* A );

//! matrix addition \f$ A + B\f$
/*!
*	\param A sparse matrix
*	\param B sparse matrix
*/
Sparse_Matrix* add( const Sparse_Matrix* A, const Sparse_Matrix* B );
//void add( Sparse_Matrix* A, const Sparse_Matrix* B );

// ! matrix multiplication \f$ A = f * A \f$
/*!
*	\param A sparse matrix
*	\param f scalar value
*/
void multiply( Sparse_Matrix* A, double f );

//! multiplication \f$ Y = A X \f$
/*!
*	\param A sparse matrix
*	\param X input column vector
*	\param Y output column vector
*/
void multiply(const Sparse_Matrix *A, const double *X, double *Y); 

//! multiplication \f$ Y = A^T X \f$
/*!
*	\param A sparse matrix
*	\param X input column vector
*	\param Y output column vector
*/
void transpose_multiply(const Sparse_Matrix *A, const double *X, double *Y); 

//! multiplication \f$ Y = (A^T A) X \f$
/*!
*	\param A sparse matrix
*	\param X input column vector
*	\param Y output column vector
*	\param tmp temporary vector \f$ tmp = A X \f$
*/
void transpose_self_multiply(const Sparse_Matrix *A, const double *X, double *Y, double *tmp); 

//! convert matrix storage
/*!
*	\param A sparse matrix
*	\param store the specified storage type
*	\param sym how to store the converted symmetric matrix. only valid, when A is symmetric
*/
Sparse_Matrix* convert_sparse_matrix_storage(const Sparse_Matrix *A, SPARSE_STORAGE store = CCS, SYMMETRIC_STATE sym = SYM_LOWER);

Sparse_Matrix* convert_sym_matrix_to_nosym_storage( Sparse_Matrix* A );

//! matrix transposition
/*!
*	\param A sparse matrix
*	\param store the specified storage type
*	\param sym how to store the converted symmetric matrix. only valid, when A is symmetric
*/
Sparse_Matrix* transpose(Sparse_Matrix* A, SPARSE_STORAGE store = CCS, SYMMETRIC_STATE sym = SYM_LOWER);

//! compute \f$ A^T A \f$
/*!
*	\param A sparse matrix
*	\param positive indicate whether the result is symmetric definite positive.
*	\param store the specified storage type
*	\param sym how to store the converted symmetric matrix. only valid, when A is symmetric
*/
Sparse_Matrix* TransposeTimesSelf(Sparse_Matrix* mat, bool positive = true,
								  SPARSE_STORAGE store = CCS, SYMMETRIC_STATE sym = SYM_LOWER, Sparse_Matrix* A = NULL );

Sparse_Matrix* TransposeTimesSelf_mt( Sparse_Matrix* mat, bool positive = true, 
									 SPARSE_STORAGE store = CCS, SYMMETRIC_STATE sym = SYM_LOWER, Sparse_Matrix* A = NULL, 
									 int n_thread = 2, double f_scalar = 1.0 );

void multiply_for_kkt_mt( Sparse_Matrix* L, Sparse_Matrix* B, Sparse_Matrix* M, int n_thread = 2, double f_scalar = 1.0 );

//! compute \f$ A A^T \f$
/*!
*	\param A sparse matrix
*	\param positive indicate whether the result is symmetric definite positive.
*	\param store the specified storage type
*	\param sym how to store the converted symmetric matrix. only valid, when A is symmetric
*/
Sparse_Matrix* SelfTimesTranspose(Sparse_Matrix* mat, bool positive = true, 
								  SPARSE_STORAGE store = CCS, SYMMETRIC_STATE sym = SYM_LOWER);


//////////////////////////////////////////////////////////////////////////
//! Sparse Matrix Class
class Sparse_Vector
{
public:

	Sparse_Vector( int n );
	Sparse_Vector( const Sparse_Matrix* A, int index, bool b_col = true );
	Sparse_Vector( double* values, int n );
	~Sparse_Vector();

public:

	inline void			fill_entry( int i, double val ) 
	{ 
		nnz_indices_.insert( i ); 
		values_[ i ] += val; 
	}

	inline void			set_entry( int i, double val )  
	{ 
		nnz_indices_.insert( i ); 
		values_[ i ] = val; 
	}

	inline double		get_entry( int i ) const 
	{ 
		if( nnz_indices_.find( i ) != nnz_indices_.end() )
			return values_[ i ]; 
		else
			return 0;
	}
	
	//double				dot_product( const Sparse_Vector* v ) const;
	double				dot_product( const double* v ) const;
	void				multiply( double x ) const;
	//Sparse_Vector*		add( const Sparse_Vector* v ) const;
	void				self_add( const Sparse_Vector* v );

protected:

	int					n_;
	double*				values_;
	std::set< int >		nnz_indices_;
};	

//@}




#endif //SPARSE_MATRIX_H

